import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
import msg_novo


###############################
 ######### Referências #########
###############################

### P1 -> Cores da página 1

p1 = {
        'titulo': '#aa8c50',
        'fundo': '#fff0cf'
}
#p2 = {
#        'box': '#f7f7f7',
#        'outline': '#b6dbd8',
#        'titulo': '#008593',
#        'fundo': '#d2ffff'
#}
p2 = {
        'box': '#f7f7f7',
        'outline': '#dba2dd',
        'titulo': '#aa62aa',
        'fundo': '#ffe6ff'
}
p3 = {
        'titulo': '#60a560',
        'fundo': '#e1ffe2',
}
p4 = {
        'titulo': '#6f6fb2',
        'fundo': '#e6e6ff',
        'grafico1':'#3c50ff',
        'grafico2':'#aa62aa'
}
p5 = {
        'titulo': '#aaa65e',
        'fundo': '#ffffcc',
        'grafico1': '#f7f7f7',
        ## grafico dois nao ta compativel
        'grafico2': '#dba2dd'
}

# Cores que não mudam entre as páginas

geral = {
    'texto': 'black',
    'botao': '#c60000',
    'fluxo': '#ff8200'
}

#######################################################################
 ############## Funções que geram o dicionário de estilos ##############
#######################################################################

def estilo_titulo (p):
    return {
        'padding-top': '20px',
        'color':p['fundo'],
        'text-shadow': \
        '-1.5px -1.5px 0 ' \
        + p['titulo'] \
        + ', 1.5px -1.5px 0 ' \
        + p['titulo'] \
        + ', -1.5px 1.5px 0 ' \
        + p['titulo'] \
        + ', 1.5px 1.5px 0 ' \
        + p['titulo']}

def estilo_subtitulos(p):
    return {
        'color' : p['titulo'],
    }

def estilo_botoes():
    return {
        'padding':'1.5rem',
        'color':geral['botao'],
        #'font-size': '2rem',
        'width': 'auto',
        'height': 'auto',
        'line-height': '2px',
        'white-space':'break-spaces',
        'font-family': 'Univers Black',
    }

def estilo_botoes_nav():
    return {
        'background-color':geral['botao'],
        'color':p1['fundo'],
        'font-size': '25px',
        'width': 'auto',
        'margin': '5px',

    }

def estilo_pagina(p):
    return {
        'padding': '50px',
        'background-color': p['fundo'],
        'color': 'black',
        'min-height': '100vh',

    }

def estilo_img():
    return {
        'widht':'160px',
        'height':'160px',
        'margin':'10px'
    }

def estilo_valor():
    return {
        'padding':'50px 10px 10px 10px',

    }

def estilo_box_valor():
    return {
        'border':'1px solid black',
        'width': '70%',
        'margin-left': 'auto',
        'margin-right': 'auto',
#        'max-width': '1200px',
        'position':'flex'

    }

def estilo_logo():
    return {
        'width':'90px',
        'height':'50px'}

def estilo_img_footer():
    return {
        'width':'25px',
        'height':'25px'}

################################
 ######### Funções Aux. #########
################################

def link_inline (pre, ancora, link, pos):
    return html.Div(
        [
#            html.Hr(),
            html.P(pre,
                   style={'display': 'inline'}),
            html.A(ancora,
                   href=link,
                   target='_blank',
                   style={
                       'color':'blue'}),
            html.P(pos,
                   style={'display': 'inline'}),
 #           html.Hr(),
        ],# className="twelve columns",
        #style={'fontSize': 18, 'padding-top': 20}
    )
#],className="row"
#),

def gen_botoes_nav():
    return html.Div(
        [html.A(html.Button(x, style=estilo_botoes_nav()), href=msg_novo.ids[x]) for x in msg_novo.titulos[1:]]
        , style={ }, className='botaoVermelho')

def gen_botao(txt, link):
    return html.Div(
        html.A(
            html.Button(
                txt,
                style=estilo_botoes(),
            ), href=link), style={'textAlign':'center'}, className='botaoVazado')


def valor_total(valor, data):
    return html.Div(children=[
        html.Div([
            html.H1(valor, style=estilo_valor()),
        ],
                 style=estilo_box_valor(), className='valorTotal'),
        html.Div([
            html.P('Valor total pago por dispensa de licitação para enfrentamento ao COVID19 | ',
                   style={'display': 'inline'}),
            html.P('Valor atualizado em: ' + data,
                   style={'display': 'inline'}),
        ],
                 ### Estilo Legandas do valor total
                 style={
                     'fontSize':'14px',
                     'color':'black',
                     'font-family': 'Univers Light'
                 })],
                    ### Estilo Valor Total + Legenda
                    style={
                        'color':geral['botao'],
                        'textAlign':'center',
                    })

#########################
 ######### Cards #########
#########################

def card_img_text(itens):
    return html.Div([html.Div(
            [html.Img(src=('assets/' + itens[item]['img'] + '.png'),
                      ### Estilo da figura dos cards
                      style={
                          'width': '120px',
                          'height': '120px',
                          'margin':'0',
                          'position': 'absolute',
                      }),
             html.Div([
                 html.H6(itens[item]['nome'], style={'font-family': 'Univers Black', 'font-size':'20px', }),
                 html.P(itens[item]['pago'], style={'font-family': 'Univers Light', }),
                 html.P(itens[item]['qtd'], style={'font-family': 'Univers Light', }),
             ],
                      ### Estilo dos textos dos cards
                 style={
                     'padding': '10px 10px 10px 40px',
                     'background-color': 'rgb(247, 247, 247)',
                     'min-width': '300px',
                     'height': '90px',
                     'border': '3px  solid  rgb(182, 219, 216)',
                     'line-height': '1',
                     'margin-left': '84px',
                 }),
            ],
        ### estilo para cada card (img+texto)
        style={
            'display': 'flex',
            'margin':'20px',
            'align-items':' center',
        }
    ) for item in itens], className="itens")

def card_grupos(grupos):
    return html.Div([html.A(html.Div(
            [html.Img(src=('assets/' + grupos[grupo]['img'] + '.png'),
                      ### Estilo da figura dos cards
                      style = {
                          'width': '130px',
                          'height': '130px',
                          'margin': '0',
                          'position': 'absolute',
                      }),
             html.Div([
                 html.H6(grupos[grupo]['nome'], style={'font-family': 'Univers Black', 'font-size': '18px', }),
                 html.P(grupos[grupo]['pago'], style={'font-family': 'Univers Light', }),
                 html.P(grupos[grupo]['valor'], style={'font-family': 'Univers Light', })
             ],
                      ### Estilo dos textos dos cards
                      style={
                          'padding': '10px 10px 10px 40px',
                          'background-color': 'rgb(247, 247, 247)',
                          'width': '275px',
                          'min-height':'90px',
                          'border': '3px  solid  rgb(219, 162, 221)',
                          'line-height': '1',
                          'margin-left': '84px',
                      }),
            ],
        ### estilo para cada card (img+texto)
        style={
            'display': 'flex',
            'margin':'20px',
            'align-items': ' center',
        }
    ), href=grupo) for grupo in grupos], className="gruposDeItens container-fluid")


titulo = html.Div([
    html.H2(msg_novo.titulos[0],style={'textAlign':'center'}),
    #html.H2(msg_novo.titulos[0][1],style={'textAlign':'center'})
],style=estilo_titulo(p1),
)
#titulo2 = html.H1(msg_novo.titulos[1],style=estilo_titulo(p2))
titulo2 = html.H1(msg_novo.titulos[1],style=estilo_titulo(p2))
titulo3 = html.H1(msg_novo.titulos[2],style=estilo_titulo(p3))
titulo4 = html.H1(msg_novo.titulos[3],style=estilo_titulo(p4))
titulo5 = html.H1(msg_novo.titulos[4],style=estilo_titulo(p5))
#titulo7 = html.H1(msg_novo.titulos[6],style=estilo_titulo(p7))

texto_intro = html.Div([ html.Div(
    msg,
    style={
        'padding-bottom':'20px',
        'font-family': 'Univers Light',
        'font-size': '18px',
        'textAlign':'center'
    }) for msg in msg_novo.texto_intro])

#texto_intro = html.Div([
#    html.Div(msg_novo.texto_intro[0],style={'padding-bottom':'20px',  'font-family': 'Univers Light', 'font-size': '18px'}),
#    html.Div(msg_novo.texto_intro[1],style={'padding-bottom':'20px',  'font-family': 'Univers Light', 'font-size': '18px'}),
#    html.Div(msg_novo.texto_intro[2],style={'padding-bottom':'20px',  'font-family': 'Univers Light', 'font-size': '18px'}),
#    ])
#

link_intro = html.Div([
    link_inline(*msg_novo.texto_link_intro),
    #html.Span([msg_novo.texto_intro[3][0]], style={'padding-bottom':'20px',  'font-family': 'Univers Light', 'font-size': '18px'}),
    #html.B(msg_novo.texto_intro[3][1])], style={'padding-bottom':'20px',  'font-family': 'Univers Black', 'font-size': '18px'})
], style={'padding-bottom':'20px',  'font-family': 'Univers Light', 'font-size': '18px'})

subtitulo1 = html.Div([
    html.H4(msg_novo.subtitulos[0]),
#    html.H4(msg_novo.subtitulos[0][1])
],style=estilo_subtitulos(p1))

x_valores = ['Serviços de Saúde', 'Assistência Social', 'Construção de Hospitais', 'Educação']
subtitulo1_valores = [html.Div(html.H2(x), style = estilo_subtitulos(p1)) for x in x_valores]

#subtitulo2_1 = html.H2(msg_novo.subtitulos[1][0],style=estilo_subtitulos(p2))
#subtitulo2_2 = html.H2(msg_novo.subtitulos[1][1],style=estilo_subtitulos(p2))

def subtitulo_0():
    return html.Div([ html.H2(
        msg,
        style=estilo_subtitulos(p1)) for msg in msg_novo.texto_intro0])

subtitulo2 = html.H4(msg_novo.subtitulos[1],style=estilo_subtitulos(p2))
subtitulo3 = html.H4(msg_novo.subtitulos[2],style=estilo_subtitulos(p3))
subtitulo4 = html.H4(msg_novo.subtitulos[3],style=estilo_subtitulos(p4))
subtitulo4_5 = html.H4('Quem contratou os 10 maiores fornecedores?',style=estilo_subtitulos(p4))
subtitulo5 = html.H4(msg_novo.subtitulos[4],style=estilo_subtitulos(p5))
#subtitulo7 = html.H4(msg_novo[6],style=estilo_subtitulos(p7))

#########


def teste(valores):
    return html.Div([html.Div(
        [html.Img(src=('assets/' + valores[orgao]['img'] + '.png'),
                  ### Estilo da figura dos cards
                      style = {
                          'width': '120px',
                          'height': '120px',
                          'margin': '0',
                          'position': 'absolute',
                      }),
         html.Div([
             html.H6(orgao, style={'font-family': 'Univers Black', 'font-size': '18px', }),
             valores[orgao]['pago']
         ],
                  ### Estilo dos textos dos cards
                      style={
                          'padding': '10px 10px 10px 40px',
                          'background-color': 'rgb(247, 247, 247)',
                          'width': '275px',
                          'min-height':'100px',
                          'border': '3px  solid  rgb(219, 162, 221)',
                          'line-height': '1',
                          'margin-left': '84px',
                      }),
        ],
        ### estilo para cada card (img+texto)
        style={
            'display': 'flex',
            'margin':'20px',
            'align-items': ' center',
        }
    ) for orgao in valores], className="gruposDeItens container-fluid")



###########################
 ######### Páginas #########
###########################

def navbar():
    return html.Header([
        dbc.Navbar(
            [
                dbc.NavbarToggler(id="navbar-toggler"),
                dbc.Collapse([html.Ul([
                    html.Li(html.A('Início', href='/#inicio', className='nav-link', style={'padding-top':'20px'}), className='nav-item active'),
#                    html.Li(html.A('O que compra?', href='#oQueCompra', className='nav-link'),
#                            className='nav-item active'),
                    html.Li(html.A('Pra que compra?', href='/#praQueCompra', className='nav-link', style={'padding-top':'20px'}),
                            className='nav-item active'),
                    html.Li(html.A('Pra quem paga?', href='/#praQuemPaga', className='nav-link', style={'padding-top':'20px'}),
                            className='nav-item active'),
                    html.Li(html.A('Que órgão paga?', href='/#queOrgaoPaga', className='nav-link', style={'padding-top':'20px'}),
                            className='nav-item active'),
                    html.Li(html.A('Quando paga?', href='/#quandoPaga', className='nav-link', style={'padding-top':'20px'}),
                            className='nav-item active'),
                    html.Li(html.A(html.Img(src='assets/covid.png', style=estilo_logo()), href='http://www.ivanmoraesfilho.com.br/combate-ao-coronavirus',
                                   className='nav-link active'), className='nav-item'),
                    html.Li(html.A(html.Img(src='assets/Vereador-Ivan-Moraes.png', style=estilo_logo()), href='http://www.ivanmoraesfilho.com.br/',
                                   className='nav-link active'), className='nav-item'),

                ], className='container navbar-nav')], id="navbar-collapse", navbar=True),
            ],
            color="light",
            dark=False,
            className="menu"
        )
    ])



##1000x94
##300x311

estilo_alerta = {
    'width': '1vw',
    'height': '1vw',
    'margin': '0',
    'textAlign':'center'

}

estilo_alerta2 = {
    'width': '45vw',
    'height': '5vw',
    'margin': '0',
}




#centralizar titulo
# centralizar light
# diminuir imgs
# aumentar texto azul

def pagina1(valores, data):
     return html.Div([
         titulo,
         valor_total(valores, data),
         html.Br(),
#         html.Div(html.Img(src='assets/faltou.png', alt='imgem com o texto: Falta Transparência',
#                           style={
#                               'width': '35vw',
#                               'height': '3.5vw'}),
#                  style={
#                      'textAlign':'center',
#                  }),
#         html.Br(),
#         html.Div([
#             html.Img(
#                 src='assets/alerta.png',
#                 alt='imagem com um sinal de exclamação',
#                 style={
#                     'width': '2vw',
#                     'height': '2vw',
#                     'display':'inline'
#                 }),
#             html.Img(
#                 src='assets/alerta.png',
#                 alt='imagem com um sinal de exclamação',
#                 style={
#                     'width': '2vw',
#                     'height': '2vw',
#                     'display':'inline'
#                 }),
#             html.Img(
#                 src='assets/alerta.png',
#                 alt='imagem com um sinal de exclamação',
#                 style={
#                     'width': '2vw',
#                     'height': '2vw',
#                     'display':'inline'
#                 })],
#             style={
#                 'textAlign':'center',
#             }),
         html.Div([
             html.H2(msg_novo.alerta),
#             html.H2(msg_novo.alerta[1])
             ],
                  style={
                      'font-family': 'Univers Black',
                      'color':'#00009e',
                      'position':'flex',
                      'padding':'1vw',
                      'textAlign':'center'

                  }),

#                         style={
#         html.Div(html.Div([
#             html.Img(src='assets/alerta.png', style=estilo_alerta),
#             html.Div(html.P(msg_novo.alerta, style={
#                 'font-family': 'Univers Black',
#                 'font-size':'20px',
#                 'color':'#fff0cf',
#                 'position':'flex',
#                 'padding':'1vw',
#                 'textAlign':'center'
#             }), style= estilo_alerta2),
#         ],  className='itens'), style={
#             'position':'flex',
#             'background-color':'blue',
#             'width':'60vw',
#             'height': '7vw',
#             'textAlign':'center'}),
#
         #         teste(valores),
         #subtitulo_0(),
#         html.Br(),
#         teste,
#         teste,
#         teste0,
#         teste0,
#         subtitulo1_valores[0],
#         valor_total(valor_saude,data),
#         subtitulo1_valores[1],
#         valor_total(valor_associal,data),
#         subtitulo1_valores[2],
#         valor_total('R$ ?? , ??' ,data),
#         subtitulo1_valores[3],
#         valor_total('R$ ?? , ??',data),
#         html.Br(),
#         texto_intro,
#         link_intro,
         subtitulo1,
         gen_botoes_nav(),
     ], style= estilo_pagina(p1), id='inicio')

#def pagina2(cards):
#    return html.Div([
#        titulo2,
#        html.Div([
#            subtitulo2_1,
#            card_img_text(cards['saude']),
#        ]),
#
#        gen_botao(*msg_novo.botoes[1][0]),
#
#        html.Div([
#        subtitulo2_2,
#        card_img_text(cards['associal']),
#            ]),
#        gen_botao(*msg_novo.botoes[1][1]),
#    ],  style=estilo_pagina(p2),id='queCompra')
#

def pagina2(cards):
    return html.Div([
        titulo2,
        subtitulo2,
        card_grupos(cards),
        gen_botao(*msg_novo.botoes[1])
    ],style=estilo_pagina(p2),id='praQueCompra')

def pagina3(tabela):
    return html.Div([
        titulo3,
        subtitulo3,
        html.Br(),
        tabela,
        html.Br(),
        gen_botao(*msg_novo.botoes[2])
    ],style=estilo_pagina(p3),id='praQuemPaga')

def pagina4(tabela, plot):
    return html.Div([
        titulo4,
        subtitulo4,
        tabela,
        html.Br(),
        subtitulo4_5,
        plot,
        html.Div(
            'Gráfico que mostra quais órgãos realizaram pagamentos aos 10 maiores fornecedores. Os oito destes dez receberam pagamentos oriundos da Secretaria de Saúde, um da Secretaria de Educação e outro  recebeu do Fundo Municipal de Assistência Social.',hidden=True),
        html.Br(),
        gen_botao(*msg_novo.botoes[3])
    ],style=estilo_pagina(p4),id='queOrgaoPaga', className='praQuemPaga')

def pagina5(plot):
    return html.Div([
        titulo5,
        subtitulo5,
        plot,
        html.Div('Gráfico de barras com todos os pagamentos. Eixo horizontal com os dias e eixo vertical com os valores. O gráfico mostra uma concentração de pagamentos entre os meses de março e abril.',hidden=True),
        html.Br(),
        gen_botao(*msg_novo.botoes[4])
    ],style=estilo_pagina(p5), id='quandoPaga')

download = html.Div([
    html.A(
        'Download Zip',
        id='download-zip',
        download = "pagamentos.csv",
        href="",
        target="_blank",
        n_clicks = 0
    )
])

def footer():
    return html.Footer(
        html.Div( className="footer container-fluid",
                  children = [
                      html.Div( className="textoFooter",
                                children=[
#                                    html.P('*Os pagamentos referentes à Construção de Hospitais de Campanha realizados pelo Gabinete de Projetos Especiais e os pagamentos realizados pela Secretaria de Educação para o fornecimento de cestas básicas e materiais de higiene não estão demonstrados no Portal da Transparência da Prefeitura do Recife.'),
#                                    html.Hr(),
                                    html.P(html.A(
                                        'Fonte: Portal da Transparência do Recife',
                                        href='http://transparencia.recife.pe.gov.br/codigos/web/despesas/despesaDetalhadaCredor.php',
                                        target='_blank'
                                    )),
                                    html.P([
                                        html.P(html.Span('Valores referentes ao pagamento dos contratos firmados com dispensa de licitação (de acordo com a Lei Nº13.979/2020 e Lei Nº8666/93 Art.24 inciso 4)')),
                                        html.P([
                                            html.Span('A tabela que utilizamos como referência você pode baixar '),
                                            html.A(
                                                'aqui',
                                                id='download-zip',
                                                download ="assets/pagamentos.csv",
                                                n_clicks = 0,
                                                title='tabela em formato csv'
                                            ),
                                            html.Span('.')
                                        ]),
                                      html.Hr(),
                                      #Esta página está licenciada sob uma licença XXXXXXXXX. |
                                      html.P([
                                          html.A('Código fonte da geração desta página',
                                                 href='https://gitlab.com/saci_perere/pagina-gastos-corona-recife', target='_blank'),
                                          html.Span(' | '),
                                          html.A('Código fonte da raspagem do Portal da Transparência e geração de CSVs base para esta página',
                                                 href='https://gitlab.com/saci_perere/gastos-corona-recife/', target='_blank')
                                          ]),
                                      html.Hr(),
                                      html.P([
                                          html.Span('Back-End por Saci Pererê ('),
                                          html.A('Contato', href='mailto:saciperere@riseup.net', target='_blank', title='saciperere@riseup.net'),
                                          html.Span(' | '),
                                          html.A('GitLab', href='https://gitlab.com/saci_perere/', target='_blank'),
                                          html.Span(') '),
                                          html.Span('e Front-End por Jennyffer de Morais ('),
                                          html.A('Contato', href='mailto:jnm.morais@gmail.com', target='_blank', title='jnm.morais@gmail.com'),
                                          html.Span(' | '),
                                          html.A('GitHub', href='https://github.com/jennyffermorais', target='_blank'),
                                          html.Span(').'),
                                          html.Hr(),
                                      ]),
                                  ]),
                                  html.Span(
                                      'Para mais informações, sugestões, elogios, críticas e dúvidas, fale com a gente!'
                                  )]),
                    html.Ul([
                        html.Li([html.Img(src="assets/e-mail.png", style = estilo_img_footer()), 'ivanmoraes@recife.pe.leg.br |']),
                        html.Li([html.Img(src="assets/telefone.png", style = estilo_img_footer()), '(81) 3301.1216 |']),
                        html.Li([html.Img(src="assets/whatsapp.png", style = estilo_img_footer()), '(81) 9.8234.5004'])
                    ], className="listaContato"),
                    html.Ul([
                        html.Li([html.Img(src="assets/twitter.png", style = estilo_img_footer()), '@ivanmoraesfilho |']),
                        html.Li([html.Img(src="assets/facebook.png", style = estilo_img_footer()), '/ivanmoraesoficial |']),
                        html.Li([html.Img(src="assets/instagram.png", style = estilo_img_footer()), '@ivanmoraesfilho'])
                    ], className="listaContato"),
                    html.A([
                        html.Img(alt="Licença Creative Commons",
                                 style={
                                     'width':'176px',
                                     'height':'61px'
                                 },
                                 src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png"
                        )
                    ], rel="license", href="http://creativecommons.org/licenses/by-nc-sa/4.0/"
                    ),
                ],
      )
    )
