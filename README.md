Página construída em Python, usando o Framework Dash para a geração de html e gráficos dinâmicos. Análise e tratamento dos dados foram realizados com a biblioteca Pandas.  

Organização do código para melhorar a leitura e contribuições serão feitas nos próximos dias. Preferi deixar o código diponível mesmo que desorganizado para a possibilidade inclusive de replicação. 
Aceitamos dicas e colaborações =D 

Referências:
- [Covid19-dashboard](https://covid19-dashboard-online.herokuapp.com/)
- [Coronavirus Contracts - Tracking Federal Purchases to Fight the Coronavirus](https://projects.propublica.org/coronavirus-contracts/)

Dependências:

pip3 install flask dash unidecode pandas dash_bootstrap_components


