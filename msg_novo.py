paginas = [
    'Início',
#    'O que compra?',
    'Pra que compra?',
    'Pra quem paga?',
    'Que órgão paga?',
    'Quando paga?'
]

titulos=[
    ['QUANTO A PREFEITURA DO RECIFE JÁ GASTOU PARA COMBATER O COVID-19?'],
#    'O QUE COMPRA?',
    'PRA QUE COMPRA?',
    'PRA QUEM PAGA?',
    'QUE ÓRGÃO PAGA?',
    'QUANDO PAGA?'
]

ids={
    'home':'home',
#    'O QUE COMPRA?':'#queCompra',
    'PRA QUE COMPRA?':'#praQueCompra',
    'PRA QUEM PAGA?':'#praQuemPaga',
    'QUE ÓRGÃO PAGA?':'#queOrgaoPaga',
    'QUANDO PAGA?':'#quandoPaga'
}

#alerta =['ATENÇÃO: Nem toda despesa relacionada à pandemia está discriminada no Portal da Transparência da Prefeitura do Recife']
alerta = ['Você encontra aqui uma forma descomplicada de acessar os gastos com a COVID-19 que estão no Portal da Transparência da Prefeitura do Recife.']

sub_gastos = [
    'Atualizado no dia ',
    '. Fonte: Portal da Transferência do Recife : ',
    'https://transparencia.recife.pe.gov.br'
]

texto_intro=[
    'Você encontra aqui uma forma descomplicada de acessar os gastos com a COVID-19 que estão no Portal da Transparência da Prefeitura do Recife.',
    'Nem tudo o que foi gasto está acessivel. A Prefeitura descumpre a lei Lei N 13.979/2020 e nāo mostra tudo o que gasta.',
    'Mas tudo que consta no Portal da Prefeitura você encontra aqui. Bem mais fácil:'
]


texto_intro_1=[
    'Esta resposta você não encontra no site da Prefeitura do Recife! NEM AQUI!',
    'Simplesmente porque a Prefeitura não cumpre com a Lei Nº13.979/2020 e Lei Nº8666/93 Art.24 inciso 4.',
    'Resultado? A gente não tem como saber o quanto a Prefeitura do Recife já gastou para Combater o Covid-19. FALTA TRANSPARÊNCIA.',
    'Por isso elaboramos este site.',
    'Aqui você encontra de forma facilitada os gastos da Secretaria de Saúde e da Secretaria de Assistência Social da Prefeitura do Recife, únicos órgão que cumprem com a legislação e prestam conta de seus gastos.',
    'Mais informações poderiam e deveriam estar aqui mas a forma insuficiente como os dados ofciais são lançados no Portal da Transparência, Portal Covid-19 e Dados Abertos, impedem a identificação de tudo o que foi contratado e pago para combater a Covid-19.'
]

texto_intro0=[
    'Esta resposta você não encontra no site da Prefeitura do Recife! NEM AQUI!',
    'Simplesmente porque a Prefeitura não cumpre com a Lei Nº13.979/2020 e Lei Nº8666/93 Art.24 inciso 4.',
    'Aqui você encontra de forma facilitada os gastos da Secretaria de Saúde e da Secretaria de Assistência Social da Prefeitura do Recife, únicos órgão que cumprem com a legislação e prestam conta de seus gastos.',
 
]

texto_intro_old = [
    'Esta resposta você não encontra no site da Prefeitura do Recife!',
    'Por isso elaboramos este site. Aqui você encontra de forma facilitada essas e outras informações sobre os gastos da Prefeitura do Recife para combater a COVID-19.',
    'Mais informações poderiam e deveriam estar aqui mas a forma insuficiente como os dados oficiais são lançados no Portal da Transparência, Portal Covid-19 e Dados Abertos, impedem a identificação de tudo o que foi contratado e pago para combater a Covid-19.'
]

texto_link_intro = ['Entre no nosso', ' site ', 'http://www.ivanmoraesfilho.com.br/combate-ao-coronavirus', 'e saiba mais.']

subtitulos = [
    [],
#    ['Gastos com ítens de Saúde','Gastos com ítens de Assistência Social'],
    ['Finalidades que mais gastam'],
    ['Lista das 10 maiores empresas fornecedoras'],#, 'Gráfico das 10 maiores empresas fornecedoras'],
    ['Principais órgãos pagadores'],
    ['Pagamentos diários']
]

botoes = [('',''),
          #[('Veja os pagamentos de todos os ítens de Saúde',''),
          #  ('Veja os pagamentos de todos os ítens de Assistência Social','')],
          ('Veja os gastos por finalidade','tabela-grupos'),
          ('Veja todas as empresas que foram pagas','tabela-fornecedores'),
          ('Veja todos os gastos por órgãos','tabela-orgaos'),
          ('Veja a lista completa de todos os pagamentos','tabela-pagamentos')
]

rodapes = [
    'Informações adicionais: A. Valores referentes ao pagamento dos contratos firmados com dispensa de licitação (de acordo com a Lei Número 13.979/2020 e Lei Número 8666/93 Art.24 inciso 4) | B. Esta página está licenciada sob uma licença XXXXXXXXX. | Desenvolvido (código fonte) por Saci Pererê (Contato | Gitlab).',
    'Fonte: Portal daTransparência do Recife : http://transparencia.recife.pe.gov.br/',
    ['Para mais informações, sugestões. elogios, críticas e dúvidas, fale com a gente!',
     'ivanmoraes@recife.pe.leg.br | (81) 3301.1216 | (81) 9.8234.5004',
     '@ivanmoraesfilho | /ivanmoraesoficial | @ivanmoraesfilho']]
