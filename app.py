import flask
import dash
import unidecode
import os
import base64
import pandas as pd
import numpy as np

import dash_table
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots

import lhtml
from data import last_update

pd.options.plotting.backend = "plotly"
local = os.path.dirname(os.path.abspath(__file__))

###################################################
  ################ Funções auxiliares ################
###################################################

def encoded_image(img_file):
    return base64.b64encode(open(img_file, 'rb').read())

def formatStr(x):
    if (x == 0):
        return 'R$ 0,00'
    if (x/1000000 > 1):
        return formatM(x)
    else:
        return formatK(x)

def formatK(x):
    return "R$ {:.2f} MIL".format(x/1000).replace('.',',')

def formatM(x):
    return "R$ {:.2f} MILHÕES".format(x/1000000).replace('.',',')

def float2real(valor):
    return 'R$ ' + format(valor, "8>,.2f").replace('.','_').replace(',','.').replace('_',',')

###################################################
 ##### Carrega DFs e gera formatos necessários #####
###################################################


#    if (pathname == '/tabela-fornecedores'):
#        return tabelas(df_table_cnpj)
#    if (pathname == '/tabela-orgaos'):
#        return tabelas(df_table_orgao)


df_p = pd.read_csv('data/dtvz_pagamentos_data.csv')
df_p = df_p[df_p['Valor pago'] > 0]
df_c = pd.read_csv('data/df_covid.csv')
df_o = pd.read_csv('data/df_orgaos.csv')
#df_i = pd.read_csv('data/df_itens.csv')
#df_i['idx'] = range(1, len(df_i) + 1)
#df_p = pd.read_csv('data/dtvz_pagamentos_data.csv')
#df_c = pd.read_csv('data/df_covid.csv', decimal=',')


### Pagamentos
df_pgs = df_p.sort_values('Valor pago', ascending=False)

df_pgs['Valor Pago'] = df_pgs['Valor pago'].apply(float2real)

## Tabela
df_pgs_table = df_pgs[['Fornecedor','CNPJ', 'Órgão', 'Grupo', 'Subelemento',
                       'Número Empenho', 'Data Pagamento', 'Valor Pago', 'Objetos Empenhados']]

## Grafico
df_pgs['Data Pagamento'] = df_pgs['Data Pagamento'].astype('datetime64[ns]')

### tabelas grupos

grupos_despesas = df_p.groupby(['Grupo', 'Subelemento'])['Valor pago'].sum().sort_values().reset_index().sort_values([
    'Grupo','Valor pago'], ascending=[True,False])
grupos_despesas['Valor pago'] = grupos_despesas['Valor pago'].apply(float2real)

### tabelas cnpjs

df_table_cnpj = df_p.groupby([
    'Fornecedor', 'CNPJ'])['Valor pago'].sum().reset_index().sort_values(
        'Valor pago', ascending=False)
df_table_cnpj['Valor pago'] = df_table_cnpj['Valor pago'].apply(formatStr)

### tabela orgaos

orgao_despesas = df_p.groupby(
    ['Órgão', 'Fornecedor', 'CNPJ']
)['Valor pago'].sum().sort_values().reset_index().sort_values(['Órgão', 'Fornecedor', 'Valor pago'], ascending=[True, True, False])
orgao_despesas['Valor pago'] = orgao_despesas['Valor pago'].apply(formatStr)



print(df_p.dtypes)
print(df_p['Órgão'].unique())
### Variaveis usadas dos DFs

#valor_total_covid = []
#assistencia = ['Fundo Municipal de Assistência Social','Fundo Municipal da Pessoa Idosa']
#valor_total_covid.append(df_p[df_p['Órgão'] == 'Secretaria de Saúde']['Valor pago'].sum())
#valor_total_covid.append(df_p[df_p['Órgão'].isin(assistencia) ]['Valor pago'].sum())
valor_total_covid = df_p['Valor pago'].sum()


### GroupBy Órgãos
#orgao_despesas = df_p.groupby('Órgão')['Valor pago'].sum().sort_values()
#orgao_covid = df_c.groupby('orgao').valor.sum().sort_values(ascending=False)
### Tabela dos 10 maiores fornecedores por SUBELEMENTO
df10mais = pd.read_csv('data/df_10mais.csv')
### DF para gráfico de flows
df_flow = {
#    'subelemento':pd.read_csv('data/subelemento_fornecedor_flow.csv'),
    'orgao':pd.read_csv('data/orgao_fornecedor_flow.csv')
    }

df_full = df_p.groupby('Grupo')['Valor pago'].sum().apply(lambda x: float2real(x))
#format(x, ",.2f").replace('.','_').replace(',','.').replace('_',','))

###################################################
############ Popular Saúde Ass. Social ############
###################################################
dict_cards={
    'itens':{
        'saude':{
            'mascara':{
                'img':'mascara',
                'nome':'Máscara',
                'pago': 'Valor Pago = R$ xx,xx',
                'qtd': 'Quantidade = XXX'},
            'ventilador':{
                'img':'ventiladores',
                'nome':'Ventilador Pulmonar',
                'pago': 'Valor Pago = R$ xx,xx',
                'qtd': 'Quantidade = XXX'},
            'alcool':{
                'img':'alcool-gel',
                'nome':'Álcool em Gel',
                'pago': 'Valor Pago = R$ xx,xx',
                'qtd': 'Quantidade = XXX'},
            'teste':{
                'img':'teste-covid',
                'nome':'Testes',
                'pago': 'Valor Pago = R$ xx,xx',
                'qtd': 'Quantidade = XXX'}
        },
        'associal':{
            'funerario':{
                'img':'servicos-funerarios',
                'nome':'Serviços Funerários',
                'pago': 'Valor Pago = R$ xx,xx',
                'qtd': 'Quantidade = XXX'},
            'cesta':{
                'img':'cesta-basica',
                'nome':'Cestas Básicas',
                'pago': 'Valor Pago = R$ xx,xx',
                'qtd': 'Quantidade = XXX'}
        }},

    'grupos':{

        'mat-hosp-epi':{
            'img':'epi',
            'nome':'Materiais Hospitalares e EPIs',
            'pago':'Valor Pago ', 'valor':df_full['MATERIAIS HOSPITALARES E EPIS'.upper()]},

        'mob-hosp':{
            'img':'equipamentos-hospitalares',
            'nome':'Mobiliários e Equipamentos Hospitalares',
            'pago':'Valor Pago ', 'valor': df_full['MOBILIÁRIOS E EQUIPAMENTOS HOSPITALARES'.upper()]},

        'gestao-hosp':{
                'img':'gestao-hospitalar',
                'nome':'Gestão Hospitalar',
                'pago':'Valor Pago ', 'valor':df_full['Gestão Hospitalar'.upper()]},

        'medicamentos':{
            'img':'medicamentos',
            'nome':'Medicamentos',
            'pago':'Valor Pago ', 'valor':df_full['Medicamentos'.upper()]},

        'obras':{
            'img':'capacete',
            'nome':'Obras e Instalações',
            'pago':'Valor Pago ', 'valor': df_full['Obras e Instalações'.upper()]},

        'refeicoes':{
            'img':'refeicoes',
            'nome':'Alimentação',
            'pago':'Valor Pago ', 'valor': df_full['Alimentação'.upper()]},

        'mat-lab':{
            'img':'mat-analise-laboratoriais',
            'nome':'Materiais e Análises Laboratoriais',
            'pago':'Valor Pago ', 'valor':df_full['Materiais e Análises Laboratoriais'.upper()]},

        'logistica':{
            'img':'transporte-logistica',
            'nome':'Logística e Transporte',
            'pago':'Valor Pago ', 'valor':df_full['Logística e Transporte'.upper()]},

        'comunicacao':{
            'img':'comunicacao',
            'nome':'Comunicação',
            'pago':'Valor Pago ', 'valor':df_full['Comunicação'.upper()]},

    }
}

estilo_ops = {
                              'width': '220px',
                              'height': '43px',
                              'margin': '0',
                              'position': 'flex',
}
#valores_titulo = {
#    'Serviços de Saúde':{
#        'img':'saude',
#        'pago' : html.H4(float2real(valor_total_covid[0]), style={'font-family': 'Univers Light', }),
#    },
#    'Assistência Social':{
#        'img':'associal',
#        'pago' : html.H4(float2real(valor_total_covid[1]), style={'font-family': 'Univers Light', }),
#    },
#    'Construção de Hospitais*':{
#        'img':'obras-instalacoes',
#        'pago' : html.Img(src='assets/ops.png',
#                          style = estilo_ops ),
#    },
#    'Educação*':{
#        'img':'educa',
#        'pago' : html.Img(src='assets/ops.png', style=estilo_ops),
#    },
#}

###################################################
 ##################### Gráficos #####################
###################################################

### Gráfico (barrras verticais) Órgão X (Dispensado;Pago)
def fontes_grafico_barra2():
    percent = orgao_despesas.values *100 / valor_total_covid
#    print (type( percent))

    fig = go.Figure()
    fig.add_trace(
        go.Bar(
            x=orgao_coviddf.index,
            y=orgao_covid.values,
            hovertemplate=
            '<i>%{x}</i><br>'
            +'<b>%{y}</b><extra></extra>',
            name='Valor Dispensado de Licitação'))
    fig.add_trace(
        go.Bar(
            x=orgao_despesas.index,
            y=orgao_despesas.values,
            hovertemplate =
#            '<br><b>Percentual nas dispensas da COVID</b>: %{text}<br>',
            '<i>%{x}</i><br>'
            +'<b>%{text}</b><br>'
            +'<b>R$ %{y}</b><extra></extra>',
            text = ['{:.2f}% do total gasto'.format(i) for i in percent],
            #"R${:.2f}".format(x)
            name='Valor Pago'))
    fig.update_layout(
        legend_bgcolor = 'rgba(0,0,0,0)',
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)',
#        font={'color':'#0'},
        xaxis={'tickangle':15},
#        hovermode='x',
        #        paper_bgcolor='#140a64',
        #        plot_bgcolor='#AAA'
    )
    return fig

def fontes_grafico_barra():
    percent = df_orgaos.valor_pago * 100 / valor_total_covid
    fig = go.Figure()
    fig.add_trace(
        go.Bar(
            x=df_orgaos.orgao,
            y=df_orgaos.valor_dispensado,
            hovertemplate=
            '<i>%{x}</i><br>'
            +'<b>%{y}</b><extra></extra>',
            name='Valor Dispensado de Licitação'))
    fig.add_trace(
        go.Bar(
            x=df_orgaos.orgao,
            y=df_orgaos.valor_pago,
            hovertemplate =
#            '<br><b>Percentual nas dispensas da COVID</b>: %{text}<br>',
            '<i>%{x}</i><br>'
            +'<b>%{text}</b><br>'
            +'<b>R$ %{y}</b><extra></extra>',
            text = ['{:.2f}% do total gasto'.format(i) for i in percent],
            #"R${:.2f}".format(x)
            name='Valor Pago'))
    fig.update_layout(
        legend_bgcolor = 'rgba(0,0,0,0)',
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)',
#        font={'color':'#0'},
        xaxis={'tickangle':15},
#        hovermode='x',
        #        paper_bgcolor='#140a64',
        #        plot_bgcolor='#AAA'
    )
    return fig

def plot_flow(categoria):
    nome = 'subelemento_nome' if (categoria == 'subelemento') else 'OU_nome'
    nodes = nodes = [
        *df_flow[categoria].credor_nome.unique(),
        *df_flow[categoria][nome].unique()
    ]
    fig = go.Figure(data=[go.Sankey(
        node = dict(
            pad = 10,
            thickness = 50,
            line = dict(color = "black", width = 0.5),
            label = nodes,
            color = 'rgba(255,130,0,1)',
            hovertemplate = '<b>%{label}</b><br><extra></extra>',
            ),

        orientation='v',
        link = dict(
            source = df_flow[categoria].source,
            target = df_flow[categoria].target,
            value = df_flow[categoria].valor_pago,
            color = 'rgba(255,130,0,0.5)',
            hovertemplate =
#            '<b>Órgão: %{source}</b><br>'
#            +'<b>Fornecedor: %{target}</b><br>'
            '<i>Valor: R$ %{value}</i><extra></extra>',
        ))])
    fig.update_layout(
        paper_bgcolor= 'rgba(0,0,0,0)',
        plot_bgcolor= 'rgba(0,0,0,0)',
        legend_bgcolor = 'rgba(0,0,0,0)',
        font_size=8
    )
    return fig

### Gráfico(stacked bar) Tempo X Pagamentos 
def pagamentos_scatter(cat='', mes=''):
    print(df_pgs.dtypes)
    fig = px.bar(
        df_pgs,
        x='Data Pagamento',
        y='Valor pago',
        #width=3000,
        color='Órgão',
        hover_data=['Fornecedor', 'Subelemento', 'Órgão'],
    )
    fig.update_layout(
        paper_bgcolor= 'rgba(0,0,0,0)',
        plot_bgcolor= 'rgba(0,0,0,0)',
        legend_bgcolor = 'rgba(0,0,0,0)',
        font={'family':'Univers Light'},
        xaxis_tickangle=45,
        xaxis_tickformat = '%B/%Y',
    )
    fig.update_xaxes(title_text="Mês")
    return fig

###################################################
 ##################### Tabelas #####################
###################################################


def tabela_orgaos_pago():

    #tab10mais = df_o.sort_values('valor_pago', ascending=False)[['orgao', 'valor_dispensado', 'valor_pago']]
    tab10mais = df_o.sort_values('valor_pago', ascending=False)[['orgao', 'valor_pago']]
    #tab10mais['porcem'] = tab10mais['valor_pago']  / tab10mais['valor_dispensado']
    #tab10mais.porcem = tab10mais.porcem.apply(lambda x: format(x, ".2%"))
    tab10mais = tab10mais[tab10mais.valor_pago > 0]
    tab10mais.valor_pago = tab10mais.valor_pago.apply(formatStr)
    #tab10mais.valor_dispensado = tab10mais.valor_dispensado.apply(formatStr)
    #tab10mais.columns = ['Órgão', 'Valor Dispensado de Licitação', 'Valor Pago', 'Porcentagem Paga']
    tab10mais.columns = ['Órgão', 'Valor Pago']
#    tab10mais['Porcentagem Paga'] = '{}%'.format(tab10mais['Valor Pago'] * 100 / tab10mais['Valor Dispensado de Licitação'])
    return html.Div([
        html.Table([
            html.Thead(
                html.Tr([html.Th(col) for col in tab10mais.columns]),
            ),
            html.Tbody([
                html.Tr([
                    html.Td(tab10mais.iloc[i][col]) for col in tab10mais.columns
                ]) for i in range(len(tab10mais))#len(dataframe))
            ])
        ]),
        html.Br()])

### Tabela 10 maiores Fornecedores;Grupo(s);Valor
def tabela_10maiores_por_grupos():
    tab10mais = df10mais[['Fornecedor', 'Grupo', 'Valor pago']]

    return html.Table([
        html.Thead(
            html.Tr([html.Th(col) for col in tab10mais.columns]),
        ),
        html.Tbody([
            html.Tr([
                html.Td(tab10mais.iloc[i][col]) for col in tab10mais.columns
            ]) for i in range(len(tab10mais))#len(dataframe))
        ])
    ])

### Tabela (itens)

### Tabela (grupos)

### Tabela geral

tabela = dash_table.DataTable(
    id='datatable-paging',
    columns=[
        {"name": i, "id": i} for i in [
#            'CNPJ',
            'Fornecedor',
            'Órgão',
#            'Descrição',
            'resume',
            'Valor pago',
            'Total Itens',
            'Quantidade',
            'Valor Unitário']
    ],#sorted(df_i.columns)
    page_current=0,
    page_size=20,
    is_focused = False,
    page_action='custom',
    style_table={
        'width':'500px',
        'color':'black'
    }
)

###################################################
 #################### Aplicação ####################
###################################################

server = flask.Flask(__name__)
app = dash.Dash(__name__,
                server=server,
                suppress_callback_exceptions=True, external_stylesheets=[dbc.themes.BOOTSTRAP],)
                #external_scripts=["https://cdn.plot.ly/plotly-locale-pt-br-latest.js"])

#app.scripts.append_script({"external_url": "https://cdn.plot.ly/plotly-locale-pt-br-latest.js"})
#app.scripts.config.serve_locally = False

app.index_string = '''
<!DOCTYPE html>
<html>
    <head>
        {%metas%}
        <title>Gastos com a COVID-19 pela Prefeitura do Recife</title>
        {%favicon%}
        {%css%}
    </head>
    <body>
        {%app_entry%}
        <div vw class="enabled">
           <div vw-access-button class="active"></div>
           <div vw-plugin-wrapper>
           <div class="vw-plugin-top-wrapper"></div>
        </div>
        <footer>
            {%config%}
            {%scripts%}
            {%renderer%}
             <script src="https://vlibras.gov.br/app/vlibras-plugin.js"></script>
            <script>new window.VLibras.Widget('https://vlibras.gov.br/app');
            </script>
            <script>Plotly.setPlotConfig({locale: 'pt-BR'})</script>
        </footer>
<!-- Matomo JS Site_2 -->
        <script type="text/javascript">
          var _paq = window._paq = window._paq || [];
          /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
          _paq.push(['trackPageView']);
          _paq.push(['enableLinkTracking']);
          (function() {
            var u="//164.90.213.55/";
            _paq.push(['setTrackerUrl', u+'matomo.php']);
            _paq.push(['setSiteId', '2']);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
          })();
        </script>
<!-- End Matomo Code -->

<!-- Matomo Image Tracker site_1-->
        <img src="https://164.90.213.55/matomo.php?idsite=1&amp;rec=1" style="border:0" alt="" />
<!-- End Matomo -->
    </body>
</html>
'''

###################################################
 ##################### Layouts #####################
###################################################

### Chamada layout das páginas
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')],
                      className='twelve columns'
)

### Layout home
home = html.Div(id='home',
                children=[
                    lhtml.pagina1(
#                         formatStr(valor_total_covid),
                        float2real(valor_total_covid),
#                        float2real(valor_total_covid[1]),
##                        valores_titulo,
#                       'R$ {:,}'.format(valor_total_covid).replace(".", "-").replace(",", ".").replace("-", ","),
#                        '28-07-2021'
                        last_update.date
                    ),
#                    lhtml.pagina2(
#                        dict_cards['itens']),
                    lhtml.pagina2(
                        dict_cards['grupos']),
                    lhtml.pagina3(
                        tabela_10maiores_por_grupos()),
                    lhtml.pagina4(
                        tabela_orgaos_pago(),
                        dcc.Graph(figure=plot_flow('orgao'))),
                    lhtml.pagina5(
                        dcc.Graph(figure=pagamentos_scatter(), config={'locale':'pt-BR'})),
                    lhtml.footer(),
                    lhtml.navbar(),
#                    html.Hr(),
#                    lhtml.rodape
])

def intro_tabelas(txt):
    return html.Div([
        html.Hr(),
        html.P('Gastos da Prefeitura do Recife com a COVID-19'),
        html.P('Visualização por: ' + txt),
        html.A(
            'Faça aqui o download desta tabela.',
            id='download-link',
            href="",
            target="_blank"
        ),
        html.Hr(),
    ], style = {
            'fontSize':'14px',
            'color':'black',
            'font-family': 'Univers Light'
        })

@app.server.route('/download-tabelas')
def download_csv():
    return flask.send_file('./data/download.csv',
                     mimetype='text/csv',
                     attachment_filename='download_gastos_covid19.csv',
                     as_attachment=True)

def tabelas(df, txt):
    return html.Div([
        html.Div([
            html.Div(className='one columns'),
            html.Div([
                intro_tabelas(txt),
                dash_table.DataTable(
                style_cell={
                    'overflow': 'hidden',
                    'textOverflow': 'ellipsis',
                    'maxWidth': 0,
                    'whiteSpace': 'normal',
                    'textAlign': 'left',
                    'fontSize':12
                },
                    style_cell_conditional=[
                        {'if': {'column_id': 'Objetos Empenhados'},
                         'width': '25%'},
                        {'if': {'column_id': 'CNPJ'},
                         'width': '12%'}
                    ],
                    id='table',
                    columns=[{"name": i, "id": i} for i in df.columns],
                    data=df.to_dict('records'),
                )]),
        ], style={
            'padding-left':'5%',
            'padding-right':'5%',
            'padding-top':'5%',
        }),
        lhtml.navbar(),
    ])

###################################################
 #################### Callbacks ####################
###################################################

@app.callback(dash.dependencies.Output('page-content', 'children'),
              [dash.dependencies.Input('url', 'pathname')])
def display_page(pathname):
    if (pathname == '/'):
        return home
    if (pathname == '/tabela-grupos'):
        return tabelas(grupos_despesas, 'Grupos/Subelementos')
#        return tabelas(df_table_full2)
    if (pathname == '/tabela-fornecedores'):
        return tabelas(df_table_cnpj, 'Fornecedores')
    if (pathname == '/tabela-orgaos'):
        return tabelas(orgao_despesas, 'Órgãos/Fornecedores')
    if (pathname == '/tabela-pagamentos'):
        return tabelas(df_pgs_table, 'Todos os Pagamentos')
    if (pathname != None and pathname[1:] in dict_cards['grupos'].keys()):
        nome_grupo = dict_cards['grupos'][pathname[1:]]['nome'].upper()
        df_grupo = df_p[
            df_p.Grupo == nome_grupo
        ]
        df_grupo['Valor Pago'] = df_grupo['Valor pago'].apply(float2real)
        df_grupo = df_grupo[['Grupo', 'Órgão', 'CNPJ','Fornecedor', 'Valor Pago', 'Objetos Empenhados']]
        df_grupo = df_grupo.sort_values(['Grupo', 'Órgão', 'Valor Pago'], ascending=[True, True, False])
        return tabelas(df_grupo, 'Grupo ' + nome_grupo)

@app.callback(
    Output('datatable-paging', 'data'),
    [Input('datatable-paging', "page_current"),
     Input('datatable-paging', "page_size")])
def update_table(page_current,page_size):
    return df_i.iloc[
        page_current*page_size:(page_current+ 1)*page_size
    ].to_dict('records')

@app.callback(
    Output("navbar-collapse", "is_open"),
    [Input("navbar-toggler", "n_clicks")],
    [State("navbar-collapse", "is_open")],
)
def toggle_navbar_collapse(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    dash.dependencies.Output('download-link', 'href'),
    [dash.dependencies.Input('table', 'data')])
def update_download_link(data):
    df = pd.DataFrame.from_dict(data)
    print('####')
    print(df)
    print(type(df))
    df.to_csv('data/download.csv', index=False, encoding='utf-8')
#    csv_string = "data:text/csv;charset=utf-8," + urllib.quote(csv_string)
    return 'download-tabelas'

if __name__ == '__main__':
    app.run_server(debug=True)
